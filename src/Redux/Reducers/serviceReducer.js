export const initialState = {
    servicesList: []
}

export const serviceReducer = (state = initialState, action) => {
    switch (action.type) {
        case "SET_SERVICE_LIST":
            return {
                servicesList: [...action.payload.data]
            }
        default:
            return state;
    }
}