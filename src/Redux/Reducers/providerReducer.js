export const initialState = {
    providersList: []
}

export const providerReducer = (state = initialState, action) => {
    switch (action.type) {
        case "SET_PROVIDER_LIST":
            return {
                providersList: [...action.payload.data, ...action.payload.included]
            }
        default:
            return state;
    }
}