import { combineReducers } from "redux";
import { serviceReducer } from './serviceReducer';
import { providerReducer } from './providerReducer';

export default combineReducers({
    serviceReducer,
    providerReducer
});