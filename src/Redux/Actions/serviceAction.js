import axios from "axios";

export const getServices = () => async (dispatch) => {
    try {
        const res = await axios({
            method: "get",
            url: "https://api.inquickerstaging.com/v3/winter.inquickerstaging.com/services"
        });

        // console.log("services response is", res);

        dispatch({
            type: "SET_SERVICE_LIST",
            payload: res.data
        })
    }
    catch (error) {
        console.log("error while getting services list");
    }
}