import axios from "axios";

export const getProviders = () => async (dispatch) => {
    try {
        const res = await axios({
            method: "get",
            url: "https://api.inquickerstaging.com/v3/winter.inquickerstaging.com/providers?include=locations%2Cschedules.location&page%5Bnumber%5D=1&page%5Bsize%5D=10"
        });

        console.log("providers response is", res);

        dispatch({
            type: "SET_PROVIDER_LIST",
            payload: res.data
        })
    }
    catch (error) {
        console.log("error while getting providers list");
    }
}