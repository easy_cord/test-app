import React from 'react'
import Service_list from './Service_list';

function Service({ servicesList, serviceClicked }) {
    return (
        <div className="list-group">
            {servicesList.length > 0 ? servicesList.map((item, index) => (
                <Service_list key={index} item={item} serviceClicked={serviceClicked} />
            )) : "Services not available."}
        </div>
    )
}


export default Service
