import React from 'react'
import dummyImage from '../assets/person.jpg';

function Provider_list({ item }) {
    return (
        <div className="card mb-3">
            <div className="row no-gutters">
                <div className="col-md-3">
                    <img src={item ?.attributes ?.["profile-image"] ? item.attributes["profile-image"] : dummyImage} className="card-img" />
                </div>
                <div className="col-md-8">
                    <div className="card-body">
                        <h5 className="card-title">{item ?.attributes ?.name ? item.attributes.name : '-'}</h5>
                        <p className="card-text">Subspecialties:</p>
                        {item ?.attributes ?.subspecialties ?.length > 0 ?
                            item.attributes.subspecialties.map((subSpec, index) => {
                                return (
                                    <li key={index}>{subSpec}</li>
                                )
                            })
                            : 'N/A'}
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Provider_list
