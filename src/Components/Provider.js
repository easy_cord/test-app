import React from 'react'
import Provider_list from './Provider_list';

function Provider({ providersList }) {
    console.log("selected providersList is", providersList);
    return (
        <div className="list-group">
            {providersList.map((item, index) => (
                <Provider_list key={index} item={item} />
            ))}
        </div>
    )
}

export default Provider
