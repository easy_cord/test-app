import React from 'react'

function Service_list({ item, serviceClicked }) {
    return (
        <a href="#" className="list-group-item list-group-item-action" onClick={() => serviceClicked(item)}>
            <div className="d-flex w-100 justify-content-between">
                <h5 className="mb-1">{item?.attributes?.name ? item.attributes.name : null}</h5>
            </div>
        </a>
    )
}

export default Service_list
