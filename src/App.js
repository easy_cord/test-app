import React, { Component } from 'react'
import { connect } from "react-redux";
import { bindActionCreators } from "redux";

import Service from './Components/Service';
import Provider from './Components/Provider';

import { getServices } from './Redux/Actions/serviceAction';
import { getProviders } from './Redux/Actions/providerAction';

class App extends Component {

  constructor(props) {
    super(props);

    this.state = {
      serviceList: [],
      providerList: []
    }
    this.getItems();
  }

  getItems = () => {
    this.props.getServices();
    this.props.getProviders();
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.serviceList.length > 0) {
      console.log("nextProps are", nextProps);
      let uniqueServices = this.removeDuplicateService(nextProps.serviceList)
      this.setState({
        serviceList: [...uniqueServices]
      })
    }
  }

  removeDuplicateService = (services) => {
    let result = services.reduce((uniqueArr, currentValue) => {
      if (!uniqueArr.some(obj => obj.id === currentValue.id)) {
        console.log(currentValue);
        uniqueArr.push(currentValue);
      }
      return uniqueArr;
    }, []);
    console.log("result is", result);
    return result;
  }



  serviceClicked = (service) => {
    console.log("clicked service is", service);
    let serviceName = service ?.attributes ?.name ? service.attributes.name : null;
    let selectedServiceProvider = [];
    if (serviceName && this.props.providerList.length > 0) {
      selectedServiceProvider = this.props.providerList.filter(provider => provider ?.attributes ?.service === serviceName || provider ?.attributes ?.subspecialties ?.includes(serviceName))
    }

    this.setState({
      providerList: [...selectedServiceProvider]
    })
  }



  render() {
    return (
      <div className="App">
        <header className="App-header">
          <div className="container-fluid">
            <nav className="navbar navbar-expand-lg navbar-light bg-light">
              <a className="navbar-brand" href="#">Navbar</a>
            </nav>
            <div className="row text-center">
              <div className="col-lg-6">
                <h1>Control</h1>
                <Service servicesList={this.state.serviceList} serviceClicked={this.serviceClicked} />
              </div>
              <div className="col-lg-6">
                <h1>Results</h1>
                <Provider providersList={this.state.providerList} />
              </div>
            </div>
          </div>
        </header>
      </div>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    serviceList: state.serviceReducer.servicesList,
    providerList: state.providerReducer.providersList
  }
}

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({ getServices, getProviders }, dispatch);
};

export default connect(mapStateToProps, mapDispatchToProps)(App)
